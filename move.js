var BONUS_HEALTH = 1;
var BONUS_WEAPON = 2;

// оси для ухода, только нормализованные вектора!
var axes = [new Vector(1,0), new Vector(0,1)];

function move(car, world){
    
    // TODO Сделать разворот или проверку на стенку
    
    outOfСorner(car, world) ||
    moveToTick(car,world) ||
        dodgingBullets(car, world) ||
        pickUpBonuces(car, world, pickClosestToMeBonus) ||
        ((world.getTick() > 100) ? sitOnCorner(car, world) : false) ||
        backBack(car);
    //moveTo(car, new Point, car.getMaxSpeed());
}

function sitOnCorner(car, world){
	// с отступами
    var corners=[new Point(60,55), new Point(60, world.getHeight()-55), new Point(world.getWidth()-60, 55), new Point(world.getWidth()-60, world.getHeight()-55)];
    var cornersAndDistances=[];
    for (var i = 0; i < corners.length; i++){
        var corner = corners[i];
        cornersAndDistances.push({distance: car.getDistanceTo(corner.x, corner.y), c:corner});
    }
    cornersAndDistances.sort(function(a,b){return a.distance - b.distance});
    moveTo(car, world, cornersAndDistances[0].c, car.misc.myMaxSpeed, 10);
    ai.log("to corner!!1")
    return true;
}

function dodgingBullets(car, world){
    // TODO уворачиваться от патронов
    var bullets = bulletsTargetsAtMe(car, world);
    if (bullets.length > 0) {
        ai.log("bulletsTargetsAtMe: " + bullets.length);
        // очерёдность уворачивания
        bullets.sort(function(a,b){
            var distanceToMe = a.bullet.getDistanceTo(car.getX(), car.getY() - b.bullet.getDistanceTo(car.getX(), car.getY()));
            var power = a.bullet.getPower() - b.bullet.getPower();
            return power * 3 + distanceToMe;
        });
        ai.log("bullets was sorted");
        for (var i = 0; i < bullets.length; i++){
            var bullet = bullets[i].bullet;
            var collideAt = bullets[i].at;
            var ticksToCollide = bullets[i].ticks;
            ai.log("collideAt:");
            ai.log(collideAt);
            
            var me = new Rectangle(carVertices(car));
            var mtv = me.mtv(new Rectangle([{x:collideAt.x, y:collideAt.y},{x:collideAt.x+1, y:collideAt.y},{x:collideAt.x, y:collideAt.y+1},{x:collideAt.x+1, y:collideAt.y+1}]))
            if (mtv){
                var axis = mtv.axis;
                var overlap = mtv.overlap;
                ai.log("axis:");
                ai.log(axis);
                ai.log("overlap:");
                ai.log(overlap);
                
                var myVector = new Point(car.getX(), car.getY());
                var moveVector = axis.multiply(overlap);
                var myMoveVector = myVector.sum(moveVector);
                var ticksToDodge = moveVector.length() / car.misc.myMaxSpeed + 10;
                ai.log("ticksToDodge")
                ai.log(ticksToDodge)
                ai.log("ticksToCollide")
                ai.log(ticksToCollide)
                if (ticksToDodge < ticksToCollide)
                {
                    // уворачиваемся от одной пули
                    moveTo(car, world, myMoveVector, car.misc.myMaxSpeed);
                    ai.log("dodging!")
                    return true;
                }                
            }
        }
    }
    return false;
}
    
// расчёт поворота
// X = x0 + (x - x0) * cos(a) - (y - y0) * sin(a);
// Y = y0 + (y - y0) * cos(a) + (x - x0) * sin(a);
// (x0, y0) — центр, точка вокруг которой нужно вращать 
function rotate(x, y, angle, rotateX, rotateY){
    return new Point(rotateX + (x - rotateX) * Math.cos(angle) - (y - rotateY) * Math.sin(angle),
            rotateY + (y - rotateY) * Math.cos(angle) + (x - rotateX) * Math.sin(angle));
}

function carVertices(car) {
    var angle = car.getAngle();
    // левая передняя точка
    var p1 = rotate(car.getX()-car.getWidth()/2, car.getY()-car.getHeight()/2, angle, car.getX(), car.getY());
	// правая передняя точка
    var p2 = rotate(car.getX()-car.getWidth()/2, car.getY()+car.getHeight()/2, angle, car.getX(), car.getY());
    // левая задняя точка
    var p3 = rotate(car.getX()+car.getWidth()/2, car.getY()-car.getHeight()/2, angle, car.getX(), car.getY());
	// правая задняя точка
    var p4 = rotate(car.getX()+car.getWidth()/2, car.getY()+car.getHeight()/2, angle, car.getX(), car.getY());
	return [p1,p2,p3,p4];
}

function hideBehind(target){
	// TODO прятаться за кем-то
}

function pickUpBonuces(car, world, func){
    // TODO проверять, что попадает в траекторию - круг, 20 градусов поворот
	var bonuses = world.getBonuses(); // Получаем массив всех бонусов
    
    if (bonuses.length) { // Если на поле есть бонусы, то будем двигаться к ним      
        car.misc.bestBonus = func(car, world);
        if (car.misc.bestBonus)
        {
            var bonusPos = new Point(car.misc.bestBonus.getX(), car.misc.bestBonus.getY());
            moveTo(car, world, bonusPos, car.misc.myMaxSpeed);    
        	ai.log("pick up bonus!")
            return true;
        }
    } else {
        if (!car.misc.bestBonus) delete car.misc.bestBonus;
    }
    
    return false;
}

function moveTo(car, world, point, speed, ticks){
    ai.log("Move to point:")
    ai.log(point)
	var angle = car.getAngleTo(point.x, point.y); // Получаем угол до бонуса
	//ai.log("move angle")
    //ai.log(angle)
    //ai.log("move speed")
    //ai.log(speed)
    
    // TODO рассчитывать траекторию движения, не врежемся ли куда-нибудь
    // едем задом, если сзади и недалеко
    if ((angle > 90 || angle < -90) && car.getDistanceTo(point.x, point.y) < 300){
        // едем задним ходом
        if (speed > 0) speed *= -1;
        //if (angle > 45)
        //{
        //    angle *= -1;
        //}
    } else {
        if (speed < 0) speed *= -1;
    }
    //ai.log("wheel angle")
    //ai.log(angle)
    car.setWheelAngle(angle); // Задаем соответствующий угол колесам
    car.setSpeed(speed); // Задаем максимальную скорость нашей машине
    //ai.log(ticks)
    //ai.log(world.getTick()+ticks)
    if (ticks) 
    {
        //ai.log("set MoveTo!")
        car.misc.moveToTick={point: point, speed: speed, toTick: world.getTick()+ticks}
    }
}

// просто ищем ближайший бонус
function pickClosestBonus(car, world){
	var bonuses = world.getBonuses(); // Получаем массив всех бонусов
    
    bonuses.sort(function(a,b){return a.distance - b.distance});
    return bonuses[0];
}

// ищем бонус, к которому я ближе, чем кто-либо
function pickClosestToMeBonus(car, world){
	var bonuses = world.getBonuses(); // Получаем массив всех бонусов
    
    bonuses.sort(function(a,b){return a.distance - b.distance;});
    
    var suitableBonuses = []
    for (var i = 0; i < bonuses.length; i++){
    	var bonus = bonuses[i];
        
        // !!! только здоровье, если есть живые и оно нам нужно :)
        // TODO Вынести нужность поворота!
        var lifeEnemies = getLifeEnemies(world);
        var lifeEnemiesCount = getLifeEnemies(world).length;
        if ( lifeEnemiesCount > 0 && !(bonus.getType() == BONUS_HEALTH && car.getHealth() >= car.misc.maxHealth - 50)){
        	ai.log("not pick up bonus")
            ai.log(bonus.getType())
            ai.log(car.getHealth())
            ai.log(car.misc.maxHealth - 50)
            continue;
        }
        
        var distanceFromUser = [];
        distanceFromUser.push({ticks:bonus.getDistanceTo(car.getX(), car.getY())/car.misc.myMaxSpeed, user: car.getId()});
        
        for (var j=0; j < lifeEnemies.length; j++){
        	var enemy = lifeEnemies[j];
            distanceFromUser.push({ticks:bonus.getDistanceTo(enemy.getX(), enemy.getY())/car.misc.maxSpeeds[enemy], user: enemy.getId()});
        }
        
        distanceFromUser.sort(function(a,b){return a.ticks-b.ticks;})
        
        if (distanceFromUser[0].user == car.getId()) {
            suitableBonuses.push(bonus);
            // интересует только один
            break;
        }
    }
    
    if (suitableBonuses.length == 0) return null;
    
    return suitableBonuses[0];
}

function moveToTick(car,world){
    if (car.misc.moveToTick) {
        
        if (isPointAtMe(car, toTick.point) || doubleEquals(world.getTick(), toTick.toTick))
        {
            delete car.misc.moveToTick;
            car.setSpeed(-car.getSpeed());
            return true;
        }
        
        var toTick = car.misc.moveToTick;
        //ai.log("moveToTick")
        ai.log(toTick)
        ai.log(toTick.point)
        moveTo(car, world, toTick.point, toTick.speed);
        return true;
    }
    return false;
}

function backBack(car, world){
		// Если же бонусов на карте нет, потихоньку отъезжаем назад
        car.setSpeed(-car.misc.myMaxSpeed*0.7);
}

function outOfСorner(car, world){
	var myPos = new Point(car.getX(),car.getY());
    delete car.misc.moveToTick;
    //ai.log(car.getSpeed().getX());
    //ai.log(car.getSpeed().getY());
    //ai.log("angle")
    //ai.log(car.getAngle());
    //ai.log(car.getAngle() % 90);
    //ai.log(car.getSpeed().getX());
    //ai.log(car.getSpeed().getY());
    //ai.log(doubleEquals(myPos.x, car.misc.lastPos.x));
    //ai.log(doubleEquals(myPos.y, car.misc.lastPos.y));
    if ((doubleEquals(car.getAngle() % 90, 0, 3) || doubleEquals(car.getAngle() % 90, 90, 3)) && Math.abs(car.getSpeed().getX()) < 0.5 && Math.abs(car.getSpeed().getY()) < 0.5 && doubleEquals(myPos.x, car.misc.lastPos.x) && doubleEquals(myPos.y, car.misc.lastPos.y)){
        ai.log("in the corner!");
        var backPixelsX=100;
        var backPixelsY=100;
    	var points = [new Point(car.getX()+backPixelsX,car.getY()+backPixelsY),
                      new Point(car.getX()-backPixelsX,car.getY()+backPixelsY),
                      new Point(car.getX()+backPixelsX,car.getY()-backPixelsY),
                      new Point(car.getX()-backPixelsX,car.getY()-backPixelsY)];
        for (var i = 0; i < points.length; i++){
            if (isPointOnField(world, points[i])){
                var speed = car.misc.myMaxSpeed;
                //ai.log("doubleEquals(car.getAngle(), 0, 3)");
                //ai.log(doubleEquals(car.getAngle(), 0, 3));
                //ai.log("doubleEquals(car.getAngle(), 90, 3)");
                //ai.log(doubleEquals(car.getAngle(), 90, 3));
                //ai.log("doubleEquals(car.getAngle(), 180, 3)");
                //ai.log(doubleEquals(car.getAngle(), 180, 3));
                //ai.log("doubleEquals(car.getAngle(), 270, 3)");
                //ai.log(doubleEquals(car.getAngle(), 270, 3));
                //ai.log("doubleEquals(car.getAngle(), 360, 3)");
                //ai.log(doubleEquals(car.getAngle(), 360, 3));
                //ai.log("car.getX()>world.getWidth()/2");
                //ai.log(car.getX()>world.getWidth()/2);
                //ai.log("car.getY()>world.getHeight()/2");
                //ai.log(car.getY()>world.getHeight()/2);
                //ai.log("carVertices(car)");
                
                // передняя левая
                if (doubleEquals(carVertices(car)[0].x, 0, 2)) speed *= -1;
                if (doubleEquals(carVertices(car)[1].x, world.getWidth, 2)) speed *= -1;
                if (doubleEquals(carVertices(car)[1].y, 0, 2)) speed *= -1;
                if (doubleEquals(carVertices(car)[0].y, world.getHeight, 2)) speed *= -1;
                
                //ai.log("speed")
                //ai.log(speed)
                //ai.log(speed)
                //ai.log(points[i].x)
                //ai.log(points[i].y)
                //ai.log("ticks "+Math.abs(car.getDistanceTo(points[i].x,points[i].y)/speed))
                moveTo(car, world, points[i], speed, Math.abs(car.getDistanceTo(points[i].x, points[i].y)/speed))
            	ai.log("out the corner!");
                return true;
            }
        }
    }
    return false;
}


