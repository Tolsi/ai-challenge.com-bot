

function bulletsTargetsAtMe(car, world){
    var targetsAtMe = [];
    var bullets = world.getBullets();

    for (var i = 0; i < bullets.length; i++){
    	var bullet = bullets[i];
    	// не мой патрон
        //ai.log("check targets at me");
        var targetAtMe = isBulletTargetAtMe(car, bullet);
        if (bullet.getId() != car.getId() && targetAtMe){
            targetsAtMe.push({bullet: bullet, at: targetAtMe.at, ticks: targetAtMe.ticks});
        }
    }
    
    return targetsAtMe;
}
            
// TODO прогнозировать, учитывая собственные передвижения
function isBulletTargetAtMe(car, bullet){
	var bulletSpeed = bullet.getSpeed();
    var nextBulletPoint = { x: bullet.getX(), y: bullet.getY() };
    //ai.log(bullet.getX()+","+ bullet.getY());
    var	ticks = 0;
    
    do {
        // проверить на пересечение со мной
        if (isPointAtMe(car, nextBulletPoint))
        {
            ai.log("Bullet of " + bullet.getId() + " collide with me through " + ticks + " ticks")
            return {at: nextBulletPoint, ticks: ticks};
        }
        ticks++;
        nextBulletPoint = { x: nextBulletPoint.x + bulletSpeed.getX(),y: nextBulletPoint.y + bulletSpeed.getY()}
    }
    while (isPointOnField(world, nextBulletPoint));
    return false;
}

function isPointOnField(world, point){
	return point.x >= 0 && point.x <= world.getWidth() &&
    	point.y >= 0 && point.y <= world.getHeight();
}

function isPointAtMe(car, point){
    var me = new Rectangle(carVertices(car));
    //ai.log("my coords:");
    //ai.log(p1+","+ p2+","+p3+","+p4);
    var p = new Point(point.x, point.y);
    
    for (var i = 0; i < axes.length; i++)
    {
		var axis = axes[i];
        //ai.log("axis");
        //ai.log(axis);
        var pr1 = me.project(axis);
        //ai.log("me");
        //ai.log(pr1);
        var pr2 = p.project(axis);
        if (!pr1.overlap(pr2)){
        	return false;
        }
    }
    return true;
}

function getLifeEnemies(world){
	var enemies = world.getEnemies();
    var lifeEmenies = [];
    for (var i = 0; i<enemies.length; i++){
    	var enemy = enemies[i];
        if (enemy.getHealth() > 0) lifeEmenies.push(enemy);
    }
    
    return lifeEmenies;
}

function analisys(car, world){
    var enemies = world.getEnemies();
    var unmovedEnemies = car.misc.notMovedEnemies;
    
    for (var i = 0; i < unmovedEnemies.length; i++) {
        var enemy = unmovedEnemies[i];
        if (!(doubleEquals(car.misc.enemiesPositions[enemy].x, enemy.getX(), 3) 
              && doubleEquals(car.misc.enemiesPositions[enemy].y, enemy.getY(), 3)
              && doubleEquals(enemy.getSpeed().getX(), 0, 5)
              && doubleEquals(enemy.getSpeed().getY(), 0, 5)))
        {
            var index = unmovedEnemies.indexOf(enemy);
            unmovedEnemies.splice(index, 1);
        }
    }
    
    // обновление коодинат врагов
    for (var i = 0; i < enemies.length; i++) {
        var enemy = enemies[i];
        car.misc.enemiesPositions[enemy] = new Point(enemy.getX(), enemy.getY());
        
        // проверка, что враг не из списка стрелял
        if (car.misc.shootedEnemies.indexOf(enemy) == -1 && enemy.getTimeAfterLastShot() < world.getTick()){
        	car.misc.shootedEnemies.push(enemy);
        }
    }
}
