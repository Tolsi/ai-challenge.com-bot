function Shape(vertices){
    this.vertices = vertices;
}
Shape.prototype.project = function(axis) {
    var min = axis.dot(this.vertices[0]);
    var max = min;
    for (var i = 0; i < this.vertices.length; i++){
    	var point = this.vertices[i];
		var p = axis.dot(point);
        if (p > max) {
            max = p;
        } else if (p < min) {
            min = p;
        }
    }
    return new Projection(min, max);
}

Shape.prototype.axes = function() {
    var axes = [];
    
    function nextVerticle(o, i){return o.vertices[i + 1 == o.vertices.length ? 0 : i + 1]}
    
    for (var i = 0; i < this.vertices.length; i++){        
        var p1 = this.vertices[i];
        var p2 = nextVerticle(this, i);
        p1 = new Point(p1.x, p1.y);
        p2 = new Point(p2.x, p2.y);
        var edge = p1.subtract(p2);
        var normal = edge.perp();
        axes.push(normal);
    }
    return axes;
}

//http://polycloud.ru/blog/38.html
Shape.prototype.mtv = function(shape) {
    var overlap = Number.MAX_VALUE;
    var smallestAxis = null;
    var axes1 = Shape.prototype.axes.call(this);
    var axes2 = shape.axes();
    for (var h = 0; h < 2; h++){
        var axes=[axes1,axes2][h];
        //ai.log("axes.length");
        //ai.log(axes.length);
        for (var i = 0; i < axes.length; i++){
            var axis = axes[i];
            var pr1 = this.project(axis);
            var pr2 = shape.project(axis);
            if (!pr1.overlap(pr2)){
                //ai.log("not overlap");
                return false;
            } else {
                var o = pr1.getOverlap(pr2);
                //ai.log("overlap:");
                //ai.log(o);
                if (o.length() < overlap)
                {
                    overlap = o.length();
                    smallestAxis = axis;
                }
            }
        }
    }
    ai.log("axis");
    ai.log(smallestAxis);
    ai.log("overlap");
    ai.log(overlap);
    return {axis:smallestAxis, overlap:overlap};
}

function Projection(from,to) {
    (!from) ? this.from = 0 : this.from = from;
    (!to) ? this.to = 0 : this.to = to;
}

Projection.prototype.length = function(){
	return Math.abs(this.to - this.from);
}

Projection.prototype.overlap = function(projection) {
    function toEnd(value, range){return {value: value, range: range}}
    var ends = [toEnd(this.from, this), toEnd(this.to, this), toEnd(projection.from, projection), toEnd(projection.to, projection)];
    ends.sort(function(a,b){return a.value - b.value});
    var intersects=[];
    for (var i = 0; i < ends.length; i++) {
    	var end = ends[i];     
        
        if (contains(intersects, end.range))
        {
            intersects.splice(intersects.indexOf(end.range), 1);
        } else {
        	intersects.push(end.range);
        }
        
        //если в списке пересекающихся прямых их две, то это нам и нужно
        if (intersects.length == 2) {
            return true;
        }
    }
    
    return false;
}

Projection.prototype.getOverlap = function(projection) {
    function toEnd(value, range){return {value: value, range: range}}
    var ends = [toEnd(this.from, this), toEnd(this.to, this), toEnd(projection.from, projection), toEnd(projection.to, projection)];
    ends.sort(function(a,b){return a.value - b.value});
    var intersects=[];
    var startOverlap = null;
    var endOverlap = null;
    
    for (var i = 0; i < ends.length; i++) {
    	var end = ends[i];     
        
        if (contains(intersects, end.range))
        {
            intersects.splice(intersects.indexOf(end.range), 1);
        } else {
        	intersects.push(end.range);
        }
        
        //если в списке пересекающихся прямых их две, то это нам и нужно
        if (intersects.length == 2) {
            startOverlap = end.value;
        } else {
        	if (startOverlap != null)
            {
                endOverlap = end.value;
            }
        }
    }
    
    return new Projection(startOverlap, endOverlap);
}

function Point(x,y) {
    Vector.call(this, x, y);
}
inherit(Point, Shape)
inherit(Point, Vector)

function Vector(x,y) {
    (!x) ? this.x = 0 : this.x = x;
    (!y) ? this.y = 0 : this.y = y;
	Shape.call(this, [{x:x, y:y}]);
}

Vector.prototype.dot = function(vector) {
    return this.x * vector.x + this.y * vector.y;
}

Vector.prototype.multiply = function(scalar) {
    return new Vector (this.x * scalar, this.y * scalar);
}

Vector.prototype.sum = function(vector) {
    return new Vector (this.x + vector.x, this.y + vector.y);
}

Vector.prototype.subtract = function(vector) {
    return new Vector (this.x - vector.x, this.y - vector.y);
}

Vector.prototype.perp = function() {
    return new Vector (-this.y , this.x);
}

Vector.prototype.normalize = function() {
    return new Vector (this.x / this.length() , this.x / this.length());
}

Vector.prototype.length = function() {
    return Math.sqrt(this.x*this.x + this.y*this.y);
}

Vector.prototype.getX = function() {
    return this.x;
}
Vector.prototype.getY = function() {
    return this.y;
}

function Rectangle(p1,p2,p3,p4) {
    Shape.call(this, [p1,p2,p3,p4]);
}
function Rectangle(points) {
    Shape.call(this, points);
}
inherit(Rectangle, Shape)
