function update(car, world) {
    try {
        if (world.getTick() == 1){
            var enemies = world.getEnemies();
            
            car.misc.notMovedEnemies = enemies.slice();
            car.misc.enemiesPositions = {};
            car.misc.shootedEnemies = [];
            
            // обновление коодинат врагов
            for (var i = 0; i < enemies.length; i++) {
                var enemy = enemies[i];
                car.misc.enemiesPositions[enemy] = new Point(enemy.getX(), enemy.getY());
            }
            
            car.misc.maxHealth = car.getHealth();
            car.misc.lastPos = new Point(car.getX(), car.getY());
            car.misc.bulletSpeed = 16.875;
            car.misc.myMaxSpeed = car.getMaxSpeed();
            car.misc.maxSpeeds = {};           
            
            for (var i = 0; i< enemies.length; i++){
                var enemy = enemies[i];
                // TODO однажды исправят enemy.getMaxSpeed()
                car.misc.maxSpeeds[enemy] = car.getMaxSpeed();
                ai.log(enemy);
                ai.log(car.misc.maxSpeeds[enemy]);
            }
            
            // нормализация векторов осей. на всякий
            for (var i = 0; i < axes.length; i++){
            	var axis = axes[i];
                axes[i] = axis.normalize();
            }
            
            ai.log("initialize was completed")
        }
        
        // на всякий
        if (world.getTick() == 5){
            var bullets=world.getBullets();
            for (var i = 0; i < bullets.length; i++){
                var bullet = bullets[i];
                if (bullet.getId()==car.getId()){
                    car.misc.bulletSpeed = bulletSpeed(bullet);
                }
            }
        }
        
        ai.log("tick")
        ai.log(world.getTick());
        ai.log("angle")
        ai.log(car.getAngle())
        
        analisys(car, world);
        if (!aim(car, world))
        {
            move(car, world);
        }
        
        // TODO сделать очередь?
        car.misc.lastPos = new Point(car.getX(), car.getY());
        } catch(e)
    {
    	ai.log(e);
    }
    
    return car;
}
