var MAX_TURREL_ANGLE_PAR_TICK = 20;
var PREMIUM = "PREMIUM";

function aim(car, world){
	var enemies = world.getEnemies(); // Получаем массив всех противников
    var enemiesCount = enemies.length; // Длинна массива - количество противников
    
    if (enemiesCount > 0) {
       return shootAtClosestEnemy(car, world);        
        // TODO первыми тех, кто стреляет в меня
    }
    
    return false;
}

function shootAtClosestEnemy(car, world){
    	var moved = false;
        var liveEnemies = getLifeEnemies(world); // Получаем массив всех противников
        // Целимся и стреляем
   		// стрелять только по стреляющим врагам? для выживания. а для профита по висякам!
    	var enemy = null;
    	// TODO сделать добивание, если 1 пуля до смерти врага и в него не летит
        if (world.getTick() > 50){
            // струляем по стоящим врагам. так больше профита будет?
            ai.log("shoot to unmoved enemy");
            enemy = closestByTurrelAngleEnemy(car, world, IntersecArrays(car.misc.notMovedEnemies, liveEnemies));
        } else {
    		enemy = closestEnemy(car, world, IntersecArrays(car.misc.shootedEnemies, liveEnemies));
        }
        ai.log("enemy 1");
        ai.log(enemy);
    	ai.log(enemy == null);
    	// TODO первыми тех, кто стреляет в нас
        if (enemy == null){        
            // если их не осталось
            enemy = closestByTurrelAngleEnemy(car, world, liveEnemies);
            ai.log("enemy 2");
    		ai.log(enemy);
            if (enemy != null && world.getTick() > 30){
                ai.log("moveTo closest not shooted enemy");
                //if (!car.misc.moveToTick)
                // TODO ТАНКУЕМ ПО ОШИБКЕ! ПОКА ЭТО КРУТО :D
                moveTo(car, world, new Point(enemy.getX(), enemy.getY()), car.getMaxSpeed(), car.getDistanceTo(enemy.getX(), enemy.getY())/car.getMaxSpeed());
            	// мы двигались
                // TODO изменить, как-то криво
                moved = true;
            }
        } 
    
    	if (enemy != null){ 
    		// Если же угол меньше пяти градусов, то стреляем
            
			var distance = car.getDistanceTo(enemy.getX(), enemy.getY());
            // стрелять на опережение, учитывая расстояние
            //ai.log("enemy.getMaxSpeed()");
            //ai.log(enemy.getMaxSpeed());
            // не работает?
            // TODO использование предсказание стоящего врага имеет мало смысла!
            var enemyLineSpeed = enemy.getSpeed();
            var enemySpeed = new Vector(enemyLineSpeed.getX(), enemyLineSpeed.getY()).length();
            if (distance > 100 && !doubleEquals(enemySpeed, 0, 2) )  
                //ai.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 500");
            	enemy = predictEnemy(car, enemy);

            if (isCanShoot(car, world, enemy)) {
                // TODO ЕСЛИ НЕТ ПРЕПЯТСТВИЙ
                shoot(car, world);
            } else { 
                rotateTurrel(car, enemy);
            }
        } else {
        	ai.log("не по кому стрелять!")
        }
    
    return moved;
}

function predictEnemy(car, enemy){
    ai.log("predict enemy")
	var distance = car.getDistanceTo(enemy.getX(), enemy.getY());
    var ticks = distance / car.misc.bulletSpeed;
    var enemyPredicted = null;
    do {
    	var enemyPredictedX = enemy.getX() + (enemy.getSpeed().getX()*ticks); 
		var enemyPredictedY = enemy.getY() + (enemy.getSpeed().getY()*ticks);
        enemyPredicted = rotate(enemyPredictedX, enemyPredictedY, enemy.getWheelAngle(), enemy.getX(), enemy.getY());

        ticks--;
    } while(!isPointOnField(world, enemyPredicted));
    return enemyPredicted;
}

function closestEnemy(car, world, enemies){
	// Переменные для поиска ближайшего противника
    var minDist = 9999;
    var minI = -1;
    var enemiesCount = enemies.length; // Длинна массива - количество противников
    
    // Ищем ближайшего противника
    for (var i = 0; i < enemiesCount; i++) {
        var e = enemies[i];
        if (e.getHealth() > 0){
            var dist = car.getDistanceTo(e.getX(), e.getY());
            if (minDist > dist) { // e.getHealth() > 0 - чтобы не целиться по мертвым
                minI = i;
                minDist = dist;
            }
        }
    }
    
    if (minI == -1) {return null;} else {return enemies[minI];}
}

function closestByTurrelAngleEnemy(car, world, enemies){
	
    // Переменные для поиска ближайшего противника
    var minAngle = 9999;
    var minI = -1;
    var enemiesCount = enemies.length; // Длинна массива - количество противников
    
    // Ищем ближайшего противника
    for (var i = 0; i < enemiesCount; i++) {
        var e = enemies[i];
        if (e.getHealth() > 0){
            var angle = car.getTurretAngleTo(e.getX(), e.getY());
            if (Math.abs(minAngle) > Math.abs(angle % 360)) { // e.getHealth() > 0 - чтобы не целиться по мертвым
                minI = i;
                minAngle = angle % 360;
            }
        }
    }
   
    if (minI == -1) {return null;} else {return enemies[minI];}
    
    /*
        enemies.sort(function(a,b){
        var aangle = car.getTurretAngleTo(a.getX(), a.getY());
        var bangle = car.getTurretAngleTo(b.getX(), b.getY());
    	return Math.abs(aangle) - Math.abs(bangle);
    });
    
    if (enemies.length == 0) {return null;} else {return enemies[0];}*/
}

function rotateTurrel(car, target){
    // Получаем угол между вектором вашего дула и вектором из вашего авто в цель
    var angle = car.getTurretAngleTo(target.getX(), target.getY()); // Получаем угол между вектором вашего дула и вектором из вашего авто в цель
 
    car.setTurretAngle(car.getTurretAngle() + angle);
}

function isCanShoot(car, world, target){
    // Получаем угол между вектором вашего дула и вектором из вашего авто в цель
	var a = car.getTurretAngleTo(target.getX(), target.getY()); // Получаем угол между вектором вашего дула и вектором из вашего авто в цель
    // Если полученный угол больше 1 градуса, то продолжаем целиться
    return Math.abs(a) <= 2;
}

function shoot(car, world){
    if (world.getTick()<20)
    {
        car.shoot(PREMIUM);
    } else {
        car.shoot();
    }
}

function bulletSpeed(bullet){
	var bulletVector = new Vector(bullet.getSpeed().getX(), bullet.getSpeed().getY());
    return bulletVector.length();
}


