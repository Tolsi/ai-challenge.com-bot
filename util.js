function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}

function doubleEquals(d1, d2, error){
    return (Math.abs(d1 - d2) < (error ? error : 0.2))
}

function IntersecArrays(A,B)
{
    var M = A.length, N = B.length, C = [];
    for (var i=0; i<M; i++)
     { var j=0, k=0;
       while (B[j]!==A[ i ] && j<N) j++;
       while (C[k]!==A[ i ] && k<C.length) k++;
       if (j!=N && k==C.length) C[C.length] = A[ i ];
     }
   return C;
}
